public class FuncoesString {
    public static void main(String[] args) {
        String str = "            Curso de Java";

       // str.charAt(0); //---> Aqui estou verificando qual a posição ou ordem das letras

       //System.out.println(str.equals("Curso de Java")); // ---> Aqui estou perguntando se essa string é a mesma da variavel str

       // System.out.println(str.startsWith("Curso")); // ---> Aqui estou perguntando se a variavel comeca com essas Strings
       // System.out.println(str.endsWith("Java")); // ---> Aqui estou perguntando se a variavel termina com essas Strings

      //  System.out.println(str.substring(3)); // --> Aqui estou pedindo para ele me mostrar a letra na posicao 3 ate o final.
      //  System.out.println(str.substring(3,6)); // ---> Aqui estou pedindo para ele me mostrar a letra de posicao 3 até a 6
      //  System.out.println(str.replace("Java", "Javaaaaaaaa")); // ---> Substitua a para "Java"por "Javaaaaaa
       // System.out.println(str.toUpperCase()); // transforma todas as letras em maiusculas
        //System.out.println(str.toLowerCase()); // transforma todas as letras em minusculas
    //    System.out.println(str.trim()); // remove os espacos do inicio e do fim
     //   System.out.println(str.length()); // Quantidade de caracteres
    }
}