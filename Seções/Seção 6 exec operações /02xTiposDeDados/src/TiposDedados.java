/*Type bit
Double (64)
Float (32)
Long (64)
Int (32)
Short (16)
Byte(8)


Character (16) = armazena apenas um valor por aspas Ex: Character C = 'A' , 'B'  --> Usar aspas simples

String = depende da quantidade de string --> Usar aspas duplas
Boolean = depende da JVM

 */


public class TiposDedados {
    public static void main(String[] args) {


        Double d = 10.3;
                Float f = 100.3f;

                Long l = 9l;
                Integer i = 122345;
                Short s =10;
                Byte b = 10;

        System.out.println("Double max " + Double.MAX_VALUE + "-- Double min " + Double.MIN_VALUE);
        System.out.println("Float max " + Float.MAX_VALUE + " -- Float min " + Float.MIN_VALUE);
                System.out.println("Long max " + Long.MAX_VALUE + "-- Long min " + Long.MIN_VALUE);
        System.out.println("Integer max " + Integer.MAX_VALUE + "-- Integer min " + Integer.MIN_VALUE);
        System.out.println("Short max " + Short.MAX_VALUE + "-- Short min " + Short.MIN_VALUE);
        System.out.println("Byte max " + Byte.MAX_VALUE + "-- Byte min " + Byte.MIN_VALUE);
    }
}
