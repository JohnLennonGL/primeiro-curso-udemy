public class INCREMxDECREM {
    public static void main(String[] args) {

        Integer Numero = 10;
//Incremento
        Numero++; /* ou */  ++Numero; /* ou */ Numero += 1; /* ou */ Numero = Numero + 1;
// Decremento
        Numero--; /* ou */  --Numero; /* ou */ Numero -= 1; /* ou */ Numero = Numero - 1;


        // Testando
        System.out.println("Incremento");
        System.out.println(Numero++);
        System.out.println(++Numero);
        System.out.println(Numero += 1);
        System.out.println(Numero = Numero + 1);

        System.out.println("Decremento");
        System.out.println(Numero--);
        System.out.println(--Numero);
        System.out.println(Numero -= 1);
        System.out.println(Numero = Numero - 1);


    }
}
