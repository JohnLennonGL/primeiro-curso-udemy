import java.util.Scanner;

public class Exercicios {
    public static void main(String[] args) {
        Integer n1;
        Integer n2;
        String op;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Informe um numero: ");
        n1 = scanner.nextInt();

        System.out.println("Informe outro numero: ");
        n2 = scanner.nextInt();

        System.out.println("Digite o qual é o operador entre(+,-,/,x): ");
        op = scanner.next();



        switch (op) {
            case "+":
                System.out.println("A soma entre " + n1 + " + " + n2 + " é igual a " + (n1 + n2));
            case "-":
                System.out.println("A subtração entre " + n1 + " - " + n2 + " é igual a " + (n1 - n2));
            case "/":
                System.out.println("A divisão entre " + n1 + " / " + n2 + " é igual a " + (n1 / n2));
            case "*":
                System.out.println("A multiplição entre " + n1 + " x " + n2 + " é igual a " + (n1 * n2));
            case "x":
                System.out.println("A multiplição entre " + n1 + " x " + n2 + " é igual a " + (n1 * n2));
            default:
                System.out.println("Operador nao reconhecido.");
        }
    }
}


